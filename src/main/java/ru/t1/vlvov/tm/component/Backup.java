package ru.t1.vlvov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.command.data.AbstractDataCommand;
import ru.t1.vlvov.tm.command.data.DataBackupLoadCommand;
import ru.t1.vlvov.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP)))
            bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            Thread.sleep(5000);
            save();
        }
    }

}
